import request from 'supertest';
import AppError from '../src/models/error.model';
import { Problem } from '../src/models/problem.model';
import app from '../src/server';

let problems: Problem[] = [];

describe('POST /issues', () => {
  it('should return 201', async () => {
    const response = (await request(app).post('/issues').send(
      [
        {
          video: 'video1',
          category: 'category1',
          userId: 'userId1',
          comment: 'comment1'
        },
        {
          video: 'video1',
          category: 'category1',
          userId: 'userId2',
          comment: 'comment2'
        },
        {
          video: 'video2',
          category: 'category21',
          userId: 'userId',
          comment: 'comment'
        },
        {
          video: 'video2',
          category: 'category22',
          userId: 'userId',
          comment: 'comment'
        },
        {
          video: 'video3',
          category: 'category31',
          userId: 'userId',
          comment: 'comment'
        }
      ]
    ));

    problems = response.body.problems;

    expect(response.status).toBe(201);
    expect(response.body.problems).toMatchObject([
      {
        category: 'category1',
        issues: [
          {
            category: 'category1',
            comment: 'comment1',
            userId: 'userId1',
            video: 'video1',
            status: 'waiting'
          },
          {
            category: 'category1',
            comment: 'comment2',
            userId: 'userId2',
            video: 'video1',
            status: 'waiting'
          },
        ],
        status: 'pending',
        video: 'video1',
      },
      {
        category: 'category21',
        issues: [
          {
            category: 'category21',
            comment: 'comment',
            userId: 'userId',
            video: 'video2',
            status: 'waiting'
          },
        ],
        status: 'pending',
        video: 'video2',
      },
      {
        category: 'category22',
        issues: [
          {
            category: 'category22',
            comment: 'comment',
            userId: 'userId',
            video: 'video2',
            status: 'waiting'
          },
        ],
        status: 'pending',
        video: 'video2',
      },
      {
        category: 'category31',
        issues: [
          {
            category: 'category31',
            comment: 'comment',
            userId: 'userId',
            video: 'video3',
            status: 'waiting'
          },
        ],
        status: 'pending',
        video: 'video3',
      },
    ]);
  });
});

describe('POST /problem/:problemId/change-status/:status', () => {
  it('Pending to ready status transition', async () => {
    expect(problems[0].status).toBe('pending');
    problems[0].issues.every(issue =>
      expect(issue.status).toBe('waiting'));
    const response = await request(app).post(`/problem/${problems[0].id}/change-status/ready`);

    problems[0] = response.body.problem;
    expect(response.status).toBe(200);
    expect(problems[0]).toEqual({
      ...problems[0],
      status: 'ready',
    });

    problems[0].issues.every(issue =>
      expect(issue.status).toBe('waiting'));
  });

  it('Ready to open status transition', async () => {
    expect(problems[0].status).toBe('ready');
    problems[0].issues.every(issue =>
      expect(issue.status).toBe('waiting'));
    const response = await request(app).post(`/problem/${problems[0].id}/change-status/open`);

    problems[0] = response.body.problem;
    expect(response.status).toBe(200);
    expect(problems[0]).toEqual({
      ...problems[0],
      status: 'open',
    });

    problems[0].issues.every(issue =>
      expect(issue.status).toBe('grouped'));
  });


  it('Bad status transition', async () => {
    expect(problems[0].status).toBe('open');
    problems[0].issues.every(issue =>
      expect(issue.status).toBe('grouped'));

    try {
      await request(app).post(`/problem/${problems[0].id}/change-status/pending`);

    } catch (error) {
      expect((error as AppError).code).toBe(400);
      expect((error as AppError).message).toEqual('Bad problem status transition: Impossible to go back')
    }

  });


});


// describe('POST /create-ticket/:thirdParty/:problemId', () => {
//   it('should return 'Hello World!'', async () => {
//     const response = await request(app).post('/issues');
//     expect(response.status).toBe(201);
//   });
// });


// describe('POST /update-ticket/:thirdParty/:problemId', () => {
//   it('should return 'Hello World!'', async () => {
//     const response = await request(app).post('/issues');
//     expect(response.status).toBe(201);
//   });
// });

// describe('POST /close-ticket/:thirdParty/:problemId', () => {
//   it('should return 'Hello World!'', async () => {
//     const response = await request(app).post('/issues');
//     expect(response.status).toBe(201);
//   });
// });