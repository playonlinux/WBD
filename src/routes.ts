import express from 'express';
import { IssuesController } from './controllers/issues.controller';
import { StatusController } from './controllers/status.controller';
import { TicketController } from './controllers/ticket.controller';
import { IssueRepository } from './infrastructure/issue.repository';
import { ProblemRepository } from './infrastructure/problem.repository';
import { DatabaseService } from './services/database.service';
import { IssueService } from './services/issues.service';
import { ProblemService } from './services/problem.service';
import { TicketService } from './services/ticket.service';
import { ThirdPartyOneController } from './third-party/one';
import { ThirdPartyTwoController } from './third-party/two';

const router = express.Router();
const issuesController = new IssuesController(
  new IssueRepository(new IssueService()),
  new ProblemRepository(new ProblemService(), new DatabaseService())
);
const statusController = new StatusController(
  new ProblemRepository(new ProblemService(), new DatabaseService())
);
const ticketController = new TicketController(new TicketService());

router.post('/issues', issuesController.createIssues.bind(issuesController));
router.post('/problem/:problemId/change-status/:status', statusController.changeStatus.bind(statusController));
router.post('/create-ticket/:thirdParty/:problemId', ticketController.createTicket.bind(ticketController));
router.post('/update-ticket/:thirdParty/:problemId', ticketController.updateTicket.bind(ticketController));
router.post('/close-ticket/:thirdParty/:problemId', ticketController.closeTicket.bind(ticketController));


const thirdPartyOneController = new ThirdPartyOneController();
router.post('/thrid-party-one/create-ticket', thirdPartyOneController.createTicket.bind(thirdPartyOneController));
router.post('/thrid-party-one/update-ticket', thirdPartyOneController.updateTicket.bind(thirdPartyOneController));
router.post('/thrid-party-one/close-ticket', thirdPartyOneController.closeTicket.bind(thirdPartyOneController));

const thirdPartyTwoController = new ThirdPartyTwoController();
router.post('/thrid-party-two/create-ticket', thirdPartyTwoController.createTicket.bind(thirdPartyTwoController));
router.post('/thrid-party-two/update-ticket', thirdPartyTwoController.updateTicket.bind(thirdPartyTwoController));
router.post('/thrid-party-two/close-ticket', thirdPartyTwoController.closeTicket.bind(thirdPartyTwoController));

export default router;