import { TableName } from "../models/database.model";
import { Issue } from "../models/issue.model";
import { Problem, ProblemStatus } from "../models/problem.model";
import { DatabaseService } from "../services/database.service";
import { ProblemService } from "../services/problem.service";

export class ProblemRepository {


  private problemService: ProblemService;
  private databaseService: DatabaseService;

  constructor(problemService: ProblemService, databaseService: DatabaseService) {
    this.problemService = problemService;
    this.databaseService = databaseService
  }


  public getProblemById(problemId: string): Problem | undefined {
    return this.databaseService.getById<Problem>(TableName.problems, problemId);

  }

  public storeProblems(problems: Problem[]) {
    this.databaseService.addItems(TableName.problems, problems);
  }

  public changeStatus(problem: Problem, newStatus: ProblemStatus) {
    if (!this.problemService.isValidStatusTransition(problem.status, newStatus)) {
      throw { code: 400, message: 'Bad problem status transition: Impossible to go back' };
    }
    problem.status = newStatus;
    problem.issues =
      problem.issues.map((issue: Issue) => ({ ...issue, status: this.problemService.nextIssueStatus(problem.status) }));

    this.databaseService.updateItem(TableName.problems, problem);
  }
}