import { Issue } from "../models/issue.model";
import { Problem } from "../models/problem.model";
import { IssueService } from "../services/issues.service";

export class IssueRepository {

  private issueService: IssueService;

  constructor(issueService: IssueService) {
    this.issueService = issueService;
  }


  createProblems(issues: Issue[]): Problem[] {
    const problems: Problem[] = this.issueService.createProblems(issues);
    // this.databaseService.addItems(TableName.problems, problems);
    return problems;
  }
}