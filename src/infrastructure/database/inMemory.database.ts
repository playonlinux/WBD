import { DatabaseItem, TableName } from "../../models/database.model";


interface Database {
  [tableName: string]: Array<any>
}

export default class InMemoryDatabase {

  private static database: Database = {
    [TableName.issues]: [],
    [TableName.problems]: [],
    [TableName.tickets]: [],
  };

  public static getById<T extends DatabaseItem>(table: TableName, id: string): T {
    return this.database[table]?.find((item: T) => item.id === id)
  }

  public static addItems<T>(table: TableName, items: T[]) {
    this.database[table]?.push(...items);
  }

  public static updateItem<T extends DatabaseItem>(table: TableName, item: T) {
    const index = this.database[table]?.findIndex((i) => i.id === item.id);
    if (index === -1) {
      return null; // Problem not found
    }
    this.database[table][index] = item;
  }
}
  // public static getIssues(): Issue[] {
  //   return this.issues;
  // }

  // public static addIssue(issue: Issue): void {
  //   this.issues.push(issue);
  // }

  // public static getProblems(): Problem[] {
  //   return this.problems;
  // }

  // public static getProblemById(problemId: string): Problem | undefined {
  //   return this.problems.find((problem) => problem.id === problemId);
  // }

  // public static addProblem(problem: Problem): void {
  //   this.problems.push(problem);
  // }
  // static addProblems(problems: Problem[]) {
  //   problems.forEach((problem: Problem) => InMemoryDatabase.addProblem(problem));
  // }

  // static updateProblem(problem: Problem) {
  //   const problemIndex = this.problems.findIndex((p) => p.id === problem.id);
  //   if (problemIndex === -1) {
  //     return null; // Problem not found
  //   }
  //   this.problems[problemIndex] = problem;
  // }

