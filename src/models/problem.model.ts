import { randomUUID } from "crypto";
import { DatabaseItem } from "./database.model";
import { Issue } from "./issue.model";

export enum ProblemStatus {
  pending = 'pending',
  ready = 'ready',
  open = 'open',
  closed = 'closed'
};

export interface Problem extends DatabaseItem {
  video: string;
  category: string;
  issues: Issue[];
  status: ProblemStatus;
}

export class Problem {
  constructor(video: string, category: string, issues: Issue[]) {
    this.id = randomUUID();
    this.video = video;
    this.category = category;
    this.issues = issues;
    this.status = ProblemStatus.pending;
  }
}