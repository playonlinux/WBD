export interface DatabaseItem {
  id: string;
}

export enum TableName {
  issues = 'issues', problems = 'problems', tickets = 'tickets'
};