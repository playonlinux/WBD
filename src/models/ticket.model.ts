export interface Ticket {
  problemId: string;
  myStatus: 'running' | 'resolved';
  count: number;
  owner: 'csTeam';
}