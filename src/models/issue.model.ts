import { randomUUID } from "crypto";
import { DatabaseItem } from "./database.model";

export enum IssueStatus {
  grouped = 'grouped',
  waiting = 'waiting'
}

export interface Issue extends DatabaseItem {
  video: string;
  category: string;
  userId: string;
  comment: string;
  status: IssueStatus;
}

export class Issue {
  constructor(video: string, category: string, userId: string, comment: string) {
    this.id = randomUUID();
    this.video = video;
    this.category = category;
    this.userId = userId;
    this.comment = comment;
    this.status = IssueStatus.waiting;
  }
}