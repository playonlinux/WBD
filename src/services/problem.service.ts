import { IssueStatus } from "../models/issue.model";
import { ProblemStatus } from "../models/problem.model";

export class ProblemService {

  public isValidStatusTransition(currentStatus: ProblemStatus, newStatus: ProblemStatus): boolean {
    const validTransitions: { [key in ProblemStatus]: ProblemStatus[] } = {
      pending: [ProblemStatus.ready],
      ready: [ProblemStatus.open],
      open: [ProblemStatus.closed],
      closed: [],
    };
    return validTransitions[currentStatus].includes(newStatus);
  }

  public nextIssueStatus(problemStatus: ProblemStatus): IssueStatus {
    return {
      [ProblemStatus.pending]: IssueStatus.waiting,
      [ProblemStatus.ready]: IssueStatus.waiting,
      [ProblemStatus.open]: IssueStatus.grouped,
      [ProblemStatus.closed]: IssueStatus.grouped,
    }[problemStatus];
  }
}

