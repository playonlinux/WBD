import InMemoryDatabase from "../infrastructure/database/inMemory.database";
import { DatabaseItem, TableName } from "../models/database.model";

export class DatabaseService {

  public addItems<T>(table: TableName, items: T[]) {
    InMemoryDatabase.addItems(table, items);
  }

  public getById<T>(table: TableName, id: string): T {
    return InMemoryDatabase.getById(table, id) as T
  }

  public updateItem<T extends DatabaseItem>(table: TableName, item: T) {
    InMemoryDatabase.updateItem<T>(table, item);
  }


}