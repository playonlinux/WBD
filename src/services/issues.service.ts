import { Issue } from "../models/issue.model";
import { Problem } from "../models/problem.model";

export class IssueService {
  createProblems(issues: Issue[]): Problem[] {
    const groupedIssues = this.groupIssues(issues);
    return Object.entries(groupedIssues).reduce((problems: Problem[], [video, categories]) => {
      return Object.entries(categories).reduce((p: Problem[], [category, issues]) => {
        return [...p, new Problem(video, category, issues)];
      }, problems)

    }, []);
  }

  private groupIssues(issues: Issue[]): GroupedIssues {
    return issues.reduce((carry: GroupedIssues, issue: Issue) => ({
      ...carry,
      [issue.video]: {
        ...carry[issue.video],
        [issue.category]: [
          ...((carry[issue.video] ?? [])[issue.category] ?? []),
          new Issue(issue.video, issue.category, issue.userId, issue.comment)
        ]

      }
    }), {});
  }



}

interface GroupedIssues {
  [video: string]: {
    [category: string]: Issue[]
  }
}