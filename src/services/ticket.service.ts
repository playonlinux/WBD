export class TicketService {
  createTicket(thirdParty: string, problemId: string) {
    // Implement logic to create ticket
  }

  updateTicket(thirdParty: string, problemId: string) {
    // Implement logic to update ticket
  }

  closeTicket(thirdParty: string, problemId: string) {
    // Implement logic to close ticket
  }
}