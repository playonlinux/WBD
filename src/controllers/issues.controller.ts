import { Request, Response } from 'express';
import { IssueRepository } from '../infrastructure/issue.repository';
import { ProblemRepository } from '../infrastructure/problem.repository';
import { Issue } from '../models/issue.model';
import { Problem } from '../models/problem.model';

export class IssuesController {
  private issueRepository: IssueRepository;
  private problemRepository: ProblemRepository;

  constructor(issueRepository: IssueRepository, problemRepository: ProblemRepository) {
    this.issueRepository = issueRepository;
    this.problemRepository = problemRepository;
  }

  createIssues(req: Request, res: Response) {
    try {
      const issues: Issue[] = (req.body instanceof Array) ? req.body : req.body.length ? [req.body] : [];
      const problems: Problem[] = this.issueRepository.createProblems(issues);
      this.problemRepository.storeProblems(problems);
      res.status(201).json({ problems });
    } catch (error: unknown) {
      console.error(error);
      res.status(500).json({ error: error });
    }
  }
}