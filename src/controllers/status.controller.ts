import { Request, Response } from 'express';
import { ProblemRepository } from '../infrastructure/problem.repository';
import AppError from '../models/error.model';
import { Problem, ProblemStatus } from '../models/problem.model';

export class StatusController {
  private problemRepository: ProblemRepository;
  constructor(problemRepository: ProblemRepository) {
    this.problemRepository = problemRepository;
  }

  changeStatus(req: Request, res: Response) {
    try {
      const { problemId, status } = req.params;
      const problem: Problem | undefined = this.problemRepository.getProblemById(problemId);
      if (!problem) {
        throw { code: 404, message: 'Problem not found' };
      }
      this.problemRepository.changeStatus(problem, status as ProblemStatus);
      res.status(200).json({ problem: this.problemRepository.getProblemById(problemId) });
    } catch (error: unknown) {
      res.status((error as AppError).code).json({ message: (error as AppError).message });
    }
  }
}