import { Request, Response } from 'express';
import { TicketService } from '../services/ticket.service';

export class TicketController {
	private ticketService: TicketService;
	constructor(service: TicketService) {
		this.ticketService = service;
	}

	createTicket(req: Request, res: Response) {
		try {
			const { thirdParty, problemId } = req.params;
			this.ticketService.createTicket(thirdParty, problemId);
			res.status(201).json({ message: 'Ticket created successfully' });
		} catch (error) {
			res.status(500).json({ error: error });
		}
	}

	updateTicket(req: Request, res: Response) {
		try {
			const { thirdParty, problemId } = req.params;
			this.ticketService.updateTicket(thirdParty, problemId);
			res.status(200).json({ message: 'Ticket updated successfully' });
		} catch (error) {
			res.status(500).json({ error: error });
		}
	}

	closeTicket(req: Request, res: Response) {
		try {
			const { thirdParty, problemId } = req.params;
			this.ticketService.closeTicket(thirdParty, problemId);
			res.status(200).json({ message: 'Ticket closed successfully' });
		} catch (error) {
			res.status(500).json({ error: error });
		}
	}
}